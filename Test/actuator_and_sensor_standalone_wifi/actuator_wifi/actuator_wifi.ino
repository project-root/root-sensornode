/*

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */
 /*
  Hack.lenotta.com
  Modified code of Getting Started RF24 Library
  It will switch a relay on if receive a message with text 1, 
  turn it off otherwise.
  EdoS

 */

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
int relay = 8;

//
// Hardware conf
//

// Set up nRF24L01 radio on SPI bus plus pins 9 & 10 

RF24 radio(9,10);

//
// Topology
//

// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
  
  //Custom
  //variable for storing button status
  int buttonState = 1; 

  //holds current state (open or closed)
  int current_state = 0; 

  //Pin numbers for inputs and output signals
  int startButton = 4; //from 11
  int limitSwitch= 2;
  int enginePin = 3; //from 9
  int ledPin = 13;
  unsigned long one = 1;
  unsigned long zero = 0;

void setup(void)
{
  //
  // Print preamble
  //
  pinMode(startButton, OUTPUT);
  pinMode(limitSwitch, INPUT);
  pinMode(enginePin, OUTPUT);
  pinMode(ledPin, OUTPUT);

  Serial.begin(57600);
  //pinMode(relay, OUTPUT);
  //digitalWrite(relay, HIGH);
  printf_begin();
  printf("\nrooT Arduino\n\r");

  //
  // Setup and configure rf radio
  //

  radio.begin();
  radio.setRetries(15,15);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.startListening();
  radio.printDetails();
  
  

}

void loop(void)
{
    printf("Starting loop\n\r");
    // if there is data ready
    if ( radio.available() )
    {
      printf("Radio Available!!\n\r");
      // Dump the payloads until we've gotten everything
      unsigned long message;
      bool done = false;
      while (!done)
      {
        // Fetch the payload, and see if this was the last one.
        done = radio.read( &message, sizeof(unsigned long) );

        // Spew it
        printf("Got message %lu...",message);
        

      if (is_message_valid(message)){
          printf("Message is valid!");
          if(message != current_state){ 
            printf("After check");

            digitalWrite(enginePin, HIGH);
            digitalWrite(ledPin, HIGH);

            //Loop until limit switch changes state
            while(digitalRead(limitSwitch) != message){
            } 
            
            digitalWrite(enginePin, LOW);
            digitalWrite(ledPin, LOW);

            //TODO: Logic should just negate current state if we have LOW or HIGH
            if (current_state == zero){
                current_state = one;
            }
            else{
              current_state = zero;

            }
          }
          else{
          printf("Message received is either not validated or Current state corresponds to the intention");
          }
      }


// Delay just a little bit to let the other unit
// make the transition to receiver
delay(20);
      }
      
      printf("Stopping to listen\n\r");
      // First, stop listening so we can talk
      radio.stopListening();

      // Send the final one back.
      radio.write( &message, sizeof(unsigned long) );
      printf("Sent response.\n\r");

      // Now, resume listening so we catch the next packets.
      radio.startListening();
    }

}

boolean is_message_valid(unsigned long message){
    if (message == one){
      return true;
    }
      if (message == zero){


      return true;
    }
    else{
      return false;
    }
}

