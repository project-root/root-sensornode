#include <Servo.h>
#include <stdlib.h>
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

//char* type = "A";//Mark as Actuator (need a better way to do this, maybe we ground a pin or something)
char* type = "S";//Mark as Sensor (need a better way to do this, maybe we ground a pin or something)
//char* type = "R";//Mark as Servo (need a better way to do this, maybe we ground a pin or something)

//Different pipes
uint64_t openChan = 0xF0F0F0F2A0LL;
uint64_t newChildAddr = 0xF0F0F0F0C0LL;
uint64_t origAddr = 0xF0F0F0F0C0LL;

uint64_t addr = origAddr;
uint64_t parent;
uint64_t child_read[4];
uint64_t child_write[4];

//Radio on pins 9 and 10
RF24 radio(9,10);

//Different Message Headers
const char depthInfo = '@';
const char childDepthInfo = '^';
const char addressInfo = '$';
const char childAddressInfo = '#';
const char childAddressAssignment = '+';
const char masterInfo = '!';
const char commandInfo = '&';
const char checkChildPathInfo = '%';
const char checkPathInfo = '*';

//Network state info
unsigned long time;
boolean networkRecieved = false;
boolean gotDepth = false;
boolean inNetwork = false;
char * subnet = "F0F0F0F0";

//const int msglen = 17;

//Node Info
char nodeID[3] = "00";
char parentID[3];
unsigned int numChildren = 0;
unsigned long depth = -1; //this should overflow to the largest value because it is unsigned


int angle = 0;
const int servoPin = 3;
Servo servo;

//SENSOR
#define voltageFlipPin1 4
#define voltageFlipPin2 5
#define sensorPin 0
int flipTimer = 1000;
unsigned long sensortime = 0;
//-----------

//ACTUATOR
//Pin numbers for inputs and output signals
//int startButton = 4; //from 11
int limitSwitch= 2;
int enginePin = 3; //from 9
int ledPin = 7;
unsigned long one = 1;
unsigned long zero = 0;
unsigned long current_state = zero; 
//-----------


void setup(void)
{
  radioSetup();
  
  //SERVO
  if(type == "R")
  {
    servo.attach(servoPin);
  }
  
  //SENSOR
  else if(type == "S")
  {
    pinMode(voltageFlipPin1, OUTPUT);
    pinMode(voltageFlipPin2, OUTPUT);
    pinMode(sensorPin, INPUT);
  }
  
  //ACTUATOR
  else if(type == "A")
  {
    pinMode(limitSwitch, INPUT);
    pinMode(enginePin, OUTPUT);
    setup_actuator();
  } 
}

void loop(void)
{
  radioListner();
  if ((type == "S") && (inNetwork)){
     if(millis() - sensortime > 10000)
     {
      char msg[15];
      sprintf(msg,"%lu",ReadSensor());
      sendCommand(msg);
      sensortime = millis();
    }
  }
}

//This method is called when there is data recieved
void processCommand(char* command)
{
  //im assuming the only messages for now are 0 and 1 (i.e open and close)
  printf("%s",command);
  unsigned long cmd;
  cmd = strtoul (command, NULL, 0); //Transforming msg from string to unsigned long
  
  if(type == "A")
  {
    actuator_manager(cmd);
  }
  else if(type == "R")
  {
    servo_manager(cmd);
  }
}

//call this method when you have data to send
void sendCommand(char* command)
{
  char message[24] = "!D";
  strcat(message,nodeID);
  strcat(message,command);
  writeToParent(message);
}

//-------------------------
//SENSOR
//-------------------------
unsigned long ReadSensor(void){
  unsigned long output;

  //CODE FOR THE MOISTURE SENSOR
  output = analogRead(sensorPin);
  return output;
}

void setSensorPolarity(boolean flip){
  if(flip){
    digitalWrite(voltageFlipPin1, HIGH);
    digitalWrite(voltageFlipPin2, LOW);
  }
  else{
    digitalWrite(voltageFlipPin1, LOW);
    digitalWrite(voltageFlipPin2, HIGH);
  }
}

//-------------------------
//Servo
//-------------------------

boolean servo_manager(unsigned long message){
  printf("Servo got message %lu\r\n",message);
  if(message == zero)
  {
    servo.write(50);
  }
  else
  {
   servo.write(150);
  }
}

//-------------------------
//ACTUATOR
//-------------------------

void setup_actuator(void){
  if(digitalRead(limitSwitch) == zero){
        digitalWrite(enginePin, HIGH);
  
        //Loop until limit switch changes state
        while(digitalRead(limitSwitch) != one){
        printf("Waiting for limit switch1\n\r");
        }
        while(digitalRead(limitSwitch) != zero){
        printf("Waiting for limit switch2\n\r");
        }      
              digitalWrite(enginePin, LOW);
        printf("Done1\n\r");
  }
  else{
        digitalWrite(enginePin, HIGH);
  
        //Loop until limit switch changes state
        while(digitalRead(limitSwitch) != zero){
        printf("Waiting for limit switch2\n\r");
        }
        digitalWrite(enginePin, LOW);
        printf("Done2\n\r");
  }
}

boolean actuator_manager(unsigned long message){
      // Spew it
      printf("Got message %lu...\n\r",message);

      if (is_message_valid(message)){
          printf("Message is valid!\n\r");
          printf("Current state: %lu\n\r",current_state);
          if(message != current_state){ 
            printf("After check\n\r");

            digitalWrite(enginePin, HIGH);
            digitalWrite(ledPin, HIGH);
            printf("After getting it high\n");

            //Loop until limit switch changes state
            while(digitalRead(limitSwitch) != message){
            printf("Waiting for limit switch\n\r");
            }
            printf("Turning engine low\n\r");            
            digitalWrite(enginePin, LOW);
            digitalWrite(ledPin, LOW);

            //TODO: Logic should just negate current state if we have LOW or HIGH
            if (current_state == zero){
                current_state = one;
            }
            else{
              current_state = zero;

            }
          }
          else{
          printf("Message received is either not validated or Current state corresponds to the intention\n\r");
          }
      }
      return true;
}

boolean is_message_valid(unsigned long message){
    if (message == one){
      return true;
    }
      if (message == zero){


      return true;
    }
    else{
      return false;
    }
}

//----- Mesh  Radio "Library" --------------------------------------------------------------------

void radioSetup(void)
{
  Serial.begin(57600);
  printf_begin();
  radio.begin();
  radio.setRetries(15,15);
  radio.setDataRate(RF24_250KBPS); // increases range
  radio.setAutoAck(true);
  radio.openReadingPipe(1,origAddr);
  radio.startListening();
}

void radioListner(void)
{
    printf("loop\n\r");
    if (!gotDepth)
    {
      //Try to make contact with the network
      char* msg = "^";
      writeToOpen(msg);
      delay(1000);
    }
    if ( radio.available() )
    {
      char message[24];
      radio.read( &message, 24 );
      processMessage(message);
      radio.startListening();
     }
     if(inNetwork)
     {
       printf("In Network\n\r");
       //check if parent is alive
       /*if(millis() - time > 10000)
       {
          printf("Sending Heartbeat");
          writeToParent("%");
          //delay(1000);
       }
       //Assume parent is dead :(, try to reconnect to network
       if(millis() - time > 20000)
       {
          printf("Assuming it's DEAD");
          gotDepth = false;
          inNetwork = false;
          networkRecieved = false;
          depth = -1;
          radio.openReadingPipe(1,origAddr);
          radio.startListening();
          //delay(1000);
       }*/
     }
      delay(1000);
}

void processMessage(char* message)
{
  // Spew it
  printf("Message Rec = %s\n\r",message);
  
  switch(message[0])
  {
    //@ = depth info
    case depthInfo:
    {
      if(!networkRecieved)
      {
        time = millis();
        networkRecieved = true;
      }
      if(!inNetwork)
      {
        //Second Char is the depth (15 max i.e 0-F)
        char tmp[2];
        sprintf(tmp,"%c",message[1]);
        tmp[1] = '/0';
        unsigned long currDepth = s_to_ull(tmp);
        if(currDepth < depth)
        {
          //Get the parent id
          strncpy(parentID,message+2,2);
          printf("%s",parentID);
          depth = currDepth;
        }
        
        //Wait for 5 seconds to give all nodes in rage adiquate time to talk, then pick the best one as the parent
        if(millis() - time > 5000 && depth < -1)
        {
          //create message
          char msg[6] = "#";
          strcat(msg,parentID);
          strcat(msg,nodeID);
          msg[5] = '\0';
          writeToOpen(msg);
          
          //increase depth to parent  + 1
          depth = depth + 1;
          gotDepth = true;
          time = millis();
        }
      }
      break;
    }
    
    //$ = address assignment
    case addressInfo:
    {
      if(!inNetwork)
      {
        
        //Get the node id and address
        strncpy(nodeID,message+1,2);
        
        char nodeAddr[11];
        sprintf(nodeAddr, "%s", subnet);
        strcat(nodeAddr,nodeID);
        nodeAddr[10] = '\0';
        addr = s_to_ull(nodeAddr);
        
        //Get the parent address
        char tmp[3];
        strncpy(tmp,message+3,2);
        tmp[2] = '\0';
        
        char parentAddr[11];
        sprintf(parentAddr, "%s", subnet);
        strcat(parentAddr,tmp);
        parentAddr[10] = '\0';
        parent = s_to_ull(parentAddr);
  
        //Write to Master saying we were added!
        //Need to send the nodeID and the Type (sensor/actuator/etc)
        char added[6] = "!A";
        strcat(added,nodeID);
        strcat(added,type);
        writeToParent(added);
        inNetwork = true;
        
        //listen pipe for parent
        radio.openReadingPipe(1,addr);
        
        //listen pipe for new children
        radio.openReadingPipe(0,openChan);
        
        radio.startListening();
      }
      break;
    }
    
    //^ = Potential child is asking for depth
    case childDepthInfo:
    {
      if(numChildren < 4 && inNetwork)
      {
        char msg[6] = "@";
        char dpt[2];
        sprintf(dpt, "%x", depth);
        strcat(msg,dpt);
        strcat(msg,nodeID);
        msg[5] = '\0';
        
        writeToNewChild(msg);
      }
        break;
    }
    
    //# = child is asking us for a address
    case childAddressInfo:
    {
       printf("child sking for address\n\r");
       printf("%s", message);
      if(message[1] == nodeID[0] && message[2] == nodeID[1] && inNetwork)
      {
        char msg[24] = "!N";
        strcat(msg,nodeID);
        msg[5] = '\0';
        writeToParent(msg);
      }
      break;
    }
    
    //+ = parent is sending info for new child addresses
    case childAddressAssignment:
    {
       //check if the nodeID matches
      if(message[1] == nodeID[0] && message[2] == nodeID[1])
      {
        printf("Adding new child\n\r");
        printf("%s", message);

        //write to new child
        char msg[7] = "$";
        
        char substr[5];
        strncpy(substr, message+3, 4);
        substr[4] = '\0';
        
        strcat(msg,substr);
        msg[6] = '\0';
        writeToNewChild(msg);
        
        //save writing address
        char nodeAddr[11];
        sprintf(nodeAddr, "%s", subnet);
        
        char tmp[3];
        strncpy(tmp,message+3,2);
        tmp[2] = '\0';
        strcat(nodeAddr,tmp);
        
        child_write[numChildren] = s_to_ull(nodeAddr);
        
        //open reading address
        sprintf(nodeAddr, "%s", subnet);
        
        strncpy(tmp,message+5,2);
        tmp[2] = '\0';
        strcat(nodeAddr,tmp);

        child_read[numChildren] = s_to_ull(nodeAddr);
        radio.stopListening();
        radio.openReadingPipe(numChildren+1,child_read[numChildren]);
        radio.startListening();
        
        printULL(child_write[numChildren]);
        printULL(child_read[numChildren]);
        
        numChildren = numChildren + 1;
        
      }
      //Not for self, send to children
      else
        writeToChildren(message);
      break;
    }
    
    //! = relay message to parent
    case masterInfo:
    {
      writeToParent(message);
      break;
    }
    
    //& = command
    case commandInfo:
    {  
      //check if the nodeID matches
      if(message[1] == nodeID[0] && message[2] == nodeID[1])
      {
        char command[20];
        strncpy(command,message+3,sizeof(command));
        processCommand(command);
      }
      //Not for self, send to children
      else
        writeToChildren(message);
      break;
    }
    
    //% = Check from Child
    case checkChildPathInfo:
    {
      writeToChildren("*");
      break;
    }
    
    //* = current path is good, reset timer!
    case checkPathInfo:
    {
      time = millis();
      break;
    }  
    
  }
}

//Write to Parent and Child (will probably never use this)
void writeToAll(char* message)
{
   writeToParent(message);
   writeToChildren(message);
}

//Write to Parent
void writeToParent(char* message)
{
   writeOut(parent,message);
}

//Write to all childs
void writeToChildren(char* message)
{
   for(int i =0; i < numChildren; i++)
     writeOut(child_write[i], message);
}

//Write to the open channel on the mesh network (used for initial adding)
void writeToOpen(char* message)
{
  writeOut(openChan,message);
}

//Write to the new child what does not have an address
void writeToNewChild(char* message)
{
  writeOut(newChildAddr,message);
}

boolean writeOut(uint64_t address, char* message)
{
  if(strlen(message) < 24)
  {
    radio.stopListening();
    radio.openWritingPipe(address);
    boolean done = false;
    int trys = 0;
    while(!done && trys < 15)
    {
      done = radio.write(message,24);
      trys = trys + 1;
      printf("%s -- %d -- %s", message, trys, done ? "True\n\r" : "Timeout\n\r");
      delay(50);
    }
    radio.startListening();
    //If we failed
    if(trys == 15)
    {
      //Assume we are out of network!
      printf("Assuming it's DEAD");
      gotDepth = false;
      inNetwork = false;
      networkRecieved = false;
      depth = -1;
      radio.openReadingPipe(1,origAddr);
      radio.startListening();
      return false;
    }
    else
      return true;
  }
  else
  {
    printf("To Many Chars\n\r");
    return false;
  }
}

uint64_t s_to_ull(char* str)
{
  uint64_t out = 0;
  uint64_t newNum = 0;
  for(int i=0; i<strlen(str); i++)
  {
    switch(str[i])
    {
      case '0':
          newNum = 0;
      break;
      case '1':
          newNum = 1;
      break;
      case '2':
          newNum = 2;
      break;
      case '3':
          newNum = 3;
      break;
      case '4':
          newNum = 4;
      break;
      case '5':
          newNum = 5;
      break;
      case '6':
          newNum = 6;
      break;
      case '7':
          newNum = 7;
      break;
      case '8':
          newNum = 8;
      break;
      case '9':
          newNum = 9;
      break;
      case 'A':
          newNum = 10;
      break;
      case 'B':
          newNum = 11;
      break;
      case 'C':
          newNum = 12;
      break;
      case 'D':
          newNum = 13;
      break;
      case 'E':
          newNum = 14;
      break;
      case 'F':
          newNum = 15;
      break;
    }
    out = out * 16 + newNum;
  }
  return out;
}

void printULL(uint64_t ll)
{
  uint64_t xx = ll/1000000000ULL;
  if (xx >0) Serial.print((long)xx);
  Serial.print((long)(ll-xx*1000000000));
}


/*
void writePayload(char* send_nodeID, char* message)
{
   char out[25];
   int numPacket;
   //int outLen;
   char numPacketsStr[4];
   
   //create initial packet
   strcpy(out,send_nodeID);
   //get number of packets (round up divison)
   numPacket = (strlen(message)+msglen-1)/msglen;
   sprintf(numPacketsStr, "%03d", numPacket);
   strcat(out,numPacketsStr);
   strcat(out,"~");
   writeToAll(out);
   
   //write rest of the message
   int pointer = 0;
   numPacket = 0;
   
   while (pointer < strlen(message))
   {
     //printf("Creating Packet");
     strcpy(out,send_nodeID);
     char tmp[msglen+1];
     if (strlen(message) - pointer < msglen)
     {
       strncpy(tmp,message+pointer,strlen(message) - pointer - 1);
       //outLen = 4 + 3 + strlen(message) - pointer - 1 + 1;
     }
     else
     {
       strncpy(tmp,message+pointer,msglen);
       //outLen = 4 + 3 + msglen + 1;
     }
     
     sprintf(numPacketsStr, "%03d", numPacket);
     strcat(out,numPacketsStr);
     strcat(out,tmp);
     //out[outLen] = '\n';
     
     writeToAll(out);
     pointer = pointer+msglen;
     numPacket++;
   }
   
}*/


