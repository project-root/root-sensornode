#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

#define voltageFlipPin1 4
#define voltageFlipPin2 5
#define sensorPin 0
int flipTimer = 1000;

RF24 radio(9,10);

const uint64_t pi_addr = 0xF0F0F0F0C1LL;

void setup(void)
{
  Serial.begin(57600);
  printf_begin();

  radio.begin();
  radio.setRetries(15,15);

  // optionally, reduce the payload size.  seems to
  // improve reliability
  //radio.setPayloadSize(8);

  radio.openWritingPipe(pi_addr);

  radio.printDetails();
  pinMode(voltageFlipPin1, OUTPUT);
  pinMode(voltageFlipPin2, OUTPUT);
  pinMode(sensorPin, INPUT);
}
void setSensorPolarity(boolean flip){
  if(flip){
    digitalWrite(voltageFlipPin1, HIGH);
    digitalWrite(voltageFlipPin2, LOW);
  }
  else{
    digitalWrite(voltageFlipPin1, LOW);
    digitalWrite(voltageFlipPin2, HIGH);
  }
}
void loop(void)
{

  unsigned long output;

  //CODE FOR THE MOISTURE SENSOR
  setSensorPolarity(true);
  delay(flipTimer);
  unsigned long val1 = analogRead(sensorPin);
  delay(flipTimer);  
  setSensorPolarity(false);
  delay(flipTimer);
  // invert the reading
  unsigned long val2 = 1023 - analogRead(sensorPin);
  output = (val1 + val2) / 2;
  //-----------------------

  bool ok = radio.write( &output, sizeof(unsigned long) );

  if (ok)
    printf("ok...");
  else
    printf("failed.\n\r");
  delay(20);

}