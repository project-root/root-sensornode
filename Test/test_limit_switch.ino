/*
Test for the limit switch - It outputs the signal coming from it
Authors: Bruno & Sai
Date: 11th Feb 2014
*/
int limit_switch = 9;


void setup() {                
  	pinMode(led, INPUT);     
	Serial.begin(9600);
}

void loop() {
  	Serial.println(digitalRead(led));   // prints the digital input signal coming from the limit_switch
  	delay(1000);  // wait for a second
}

