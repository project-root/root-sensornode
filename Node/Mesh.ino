#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
#include <stdlib.h>
#include <SPI.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>

// watchdog interrupt
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
} 

//----- Mesh  Radio Library --------------------------------------------------------------------
//Radio on pins 9 and 10
RF24 radio(9,10);

void radioSetup(void)
{
    printf_begin();
    radio.begin();
    radio.setRetries(15,15);
    radio.setDataRate(RF24_250KBPS); // increases range
    radio.setCRCLength(RF24_CRC_16);
    radio.setAutoAck(true);
    radio.enableAckPayload();
    radio.startListening();
    randomSeed(analogRead(0));
    
    //Open Initial Reading Pipe
    
    //Generate NodeID
    //
    // NodeID = "0" + RRRR
    //
    // The RRRR is the random number, 0 is the Pipe for the parent.
    //
    /////////////////

    generateID(nodeID);
    
    //
    //
    //    MAKING HUGE ASSUMPTION THAT TWO NODES WONT PICK THE SAME ADDRESS!
    //
    //    1 in 4 BILLION chance of collision
    //
    //
    
    printf("NodeID:");
    print_ull(b_to_ull(nodeID,4));
    
    //Create the 5 Byte Address
    addr = (b_to_ull(nodeID,4) << 8) + 0;
    
    //open pipe for parent
    radio.openReadingPipe(1,addr);
    //Open reading pipe for new children
    radio.openReadingPipe(0,newChan);
    
    //open pipes for all future children
    for(int i = 0; i < 4; i++)
    {
      child_read[i] = addr+i+1;
      radio.openReadingPipe(i+2,child_read[i]);
    }
}


// Main Loop-----------------------------------------------------------------------------------------------------------

void radioListner(void)
{ 
   //Send Readings
   //printf("sensorTime: %lu  -- Limit:%lu\n\r", sensorTime, sensorCheckTime*1); 
   if ((inNetwork) && (sensorTime >= sensorCheckTime)){
      sensorTime = 0;
      byte message[27];
      //Write readings into buffer
      getReadings(message);
      //Write to parent - if we fail then we are out of network!
      if(!(writeToParent(message)))
         networkStateTime = 0;
      //send output brodcast
      //check if we have room to add new child and send the broadcast
      if(numChildren < maxChildren)
      {
        byte output[1];
        output[0] = imOn;
        writeToNew(output);
      }
   }
   
   //delay 1 second for children
   delay(1000);

   //process any messages
   while ( radio.available() )
   {
        //Read the message and send it to the processor function
        byte message[32];
        radio.read( &message, 32 );
        
        //false means we can process any message, not just special parent packets
        processMessage(message,false);
   }
   
   
   //printf("loop\n\r");
   
   //keep checking network status
   if(inNetwork)
   {
      checkNetworkState();
      noLight();
      
      // disable ADC
      byte adcsra_save = ADCSRA;
      ADCSRA = 0;
      radio.powerDown();
      
      //sleep
      while( (networkStateTime <= networkCheckTime) && (checkTime <= networkCheckTime) && (sensorTime <= sensorCheckTime))
      {       
          //SLEEP MODE

          // clear various "reset" flags
          MCUSR = 0;      
          // allow changes, disable reset
          WDTCSR = bit (WDCE) | bit (WDE);
          // set interrupt mode and an interval 
          //WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 8 seconds delay
          WDTCSR = bit (WDIE) | bit (WDP2) | bit (WDP1);    // set WDIE, and 1 seconds delay
          wdt_reset();  // pat the dog
          
          set_sleep_mode (SLEEP_MODE_PWR_DOWN);
          
          // turn off brown-out enable in software
          MCUCR = bit (BODS) | bit (BODSE);
          MCUCR = bit (BODS); 
          //sleep_mode();
          delay(1000);
          
          //Increment Times
          networkStateTime++;
          checkTime++; 
          sensorTime++;
          //printf("sensor: %lu, loops left %lu\n\r",sensorTime,(sensorCheckTime-sensorTime));
          //printf("network: %lu, loops left %lu\n\r",networkStateTime,(networkCheckTime-networkStateTime));
          //printf("child: %lu, loops left %lu\n\r",checkTime,(networkCheckTime-checkTime));

       }
       //Enable ADC
       ADCSRA = adcsra_save;
       radio.powerUp();
       //solidLight();
    }
}
//---------------------------------------------------------------------------------------------------------------------


//Main Processor------------------------------------------------------------------------------------------------------
boolean processMessage(byte* message, boolean check)
{ 
  //Get the message Header
  byte header = message[0];
  
  //Get from who the message was sent
  byte messageNodeId[4];
  messageNodeId[0] = message[1];
  messageNodeId[1] = message[2];
  messageNodeId[2] = message[3];
  messageNodeId[3] = message[4];
  
  //get the Header and the Node that sent the message
  printf("Rec Header: %u\n\r",header);
  //printf("Message NodeID: ");
  uint64_t talkBack = (b_to_ull(messageNodeId,4));
  //print_ull(talkBack);
  
  //Get the pipe to write back to the requestor (always ends in 0)
  talkBack = (talkBack << 8) + 0;
  //printf("talkBack = ");
  //print_ull(talkBack);
  
   //find an write slot
   int writeSlot = -1;
   for(int i = 0; i < 4; i++)
   {
     if(child_write[i] == talkBack)
     {
       writeSlot = i;
       break;
     }
   }
  
  //process messages
  
   
   //////////////////////////////////////////////////////////////////////////
  // 1 = request to join network
  //
  //
  // Packet Structure = B       BBBB
  //                    Header  NodeID   
  //
  //////////////////////////////////////////////////////////////////////////
  if(header == imOn && !inNetwork && !check)
  {
    //Blink led
    flashLight();
    byte output[5];
    output[0] = childDepthInfo;
    output[1] = nodeID[0];
    output[2] = nodeID[1];
    output[3] = nodeID[2];
    output[4] = nodeID[3];
    writeToOpen(output);
  }
  //////////////////////////////////////////////////////////////////////////
  // 2 = parent pipe info
  //
  //
  // Packet Structure = B       BBBB     BBBBB         B
  //                    Header  NodeID   ParentPipeID  Depth    
  //
  //////////////////////////////////////////////////////////////////////////
  
  //make sure we are not already in a network
  else if(header == depthInfo && !inNetwork && !check)
  {
      //Get the parent's pipe 
      byte parentPipeID[5];
      parentPipeID[0] = message[5];
      parentPipeID[1] = message[6];
      parentPipeID[2] = message[7];
      parentPipeID[3] = message[8];
      parentPipeID[4] = message[9];
      
      parent = b_to_ull(parentPipeID,5);
      
      printf("Par Pipe:");
      print_ull(parent);
      
      //Get depth
      depth = message[10];
      
      //Get sensor time
      networkStateFromParent = message[11];
      sensorCheckTime = int((networkStateFromParent * wdtCalibration));
      networkCheckTime = sensorCheckTime*2;
      //Open reading pipe for new children
      radio.openReadingPipe(0,openChan);
      
      //join network
      inNetwork = true;
      
      //Turn on red led
      solidLight();
      
      //Delay state check
      networkStateTime = 0;

      return true;
  }
  
   //////////////////////////////////////////////////////////////////////////
  // 1 = child reqest for pipe
  //
  //
  // Packet Structure = B       BBBB        
  //                    Header  Child NodeID
  //
  //////////////////////////////////////////////////////////////////////////
  else if(header == childDepthInfo && inNetwork && numChildren < maxChildren && !check)
  {
     //-------------------------------------------------------------------------
     //- 2 = depth info
     //-
     //- Send the child the depth info of this node
     //-
     //- Packet Structure = B       BBBB     BBBBB         B
     //-                    Header  NodeID   ParentPipeID  Depth     
     //-
     //------------------------------------------------------------------------
     byte output[12];
     output[0] = depthInfo;
     
     //output our node ID
     output[1] = nodeID[0];
     output[2] = nodeID[1];
     output[3] = nodeID[2];
     output[4] = nodeID[3];
     
     //find an empty slot
     int slot = -1;
     for(int i = 0; i < sizeof(child_write)/sizeof(uint64_t); i++)
     {
       if(child_write[i] == 0)
       {
         slot = i;
         break;
       }
     }
     
     //append the slot address
     output[5] = (child_read[slot] >> 32) & 255;
     output[6] = (child_read[slot] >> 24) & 255;
     output[7] = (child_read[slot] >> 16) & 255;
     output[8] = (child_read[slot] >>  8) & 255;
     output[9] = (child_read[slot] >>  0) & 255;
     
     //append the dpeth
     output[10] = depth;
     
     //append the dpeth
     output[11] = networkStateFromParent;
     
     //add child to write_child
     child_write[slot] = talkBack;
     
     //add to status
     child_status[slot] = true;
     
     //write back to child
     writeOut(talkBack,output,false);
     
     return true;
  }
  //////////////////////////////////////////////////////////////////////////
  // 5 = trx success
  //
  //
  // Packet Structure = B       BBBB    BBBBBBB
  //                    Header  NodeID  Time
  //                    
  //////////////////////////////////////////////////////////////////////////
  
  else if(header == checkChildPathInfo && inNetwork)
  {
     networkStateTime = 0;
     noLight();
     
     byte timearray[8];
     timearray[0] = message[5];
     timearray[1] = message[6];
     timearray[2] = message[7];
     timearray[3] = message[8];     
     timearray[4] = message[9];
     timearray[5] = message[10];
     timearray[6] = message[11];
     timearray[7] = message[12];
     uint64_t time = (b_to_ull(timearray,8));
     
     print_ull(prevtime);
     
     print_ull(prevtime + (sensorCheckTime*8));
     
     print_ull(time);
     
     print_ull(time - prevtime);
     
     prevtime = time;
     
     return true;
  }
  
   //////////////////////////////////////////////////////////////////////////
  // 3 = forward message to parent
  //
  //
  // Packet Structure = B        BBBBBBBBBBBBBBBBBBB
  //                    Header   Message
  //
  //////////////////////////////////////////////////////////////////////////
  
  else if(header == masterInfo && inNetwork && !check && writeSlot != -1)
  {  
     
     //Makr child as talked
     child_status[writeSlot] = true;
     
     //Replace ID with self
     message[1] = nodeID[0];
     message[2] = nodeID[1];
     message[3] = nodeID[2];
     message[4] = nodeID[3];
     
     //Forward to parent
     writeToParent(message);
     
     //Reply back to child with success
     byte output[5];
     output[0] = checkChildPathInfo;
     output[1] = nodeID[0];
     output[2] = nodeID[1];
     output[3] = nodeID[2];
     output[4] = nodeID[3];
     writeOut(child_write[writeSlot],output,false);
     
     return true;
  }
  
   //////////////////////////////////////////////////////////////////////////
  // 7 = network is broken!
  //
  //
  // Packet Structure = B
  //                    Header
  //
  //////////////////////////////////////////////////////////////////////////
  else if((header == networkBroken) && inNetwork)
  {
    printf("DEAD - BROKEN\n\r");
    //Open reading pipe for network
    radio.openReadingPipe(0,newChan);
      
    //write back to parent saying we recieved the dead message
    byte output[5];
    output[0] = networkBrokenConfirm;
    output[1] = nodeID[0];
    output[2] = nodeID[1];
    output[3] = nodeID[2];
    output[4] = nodeID[3];
    writeToParent(output);
    
    //write to children with "dead"
    tellChildrenImDead();
    return true;
  }
  
     //////////////////////////////////////////////////////////////////////////
  // 8 = Child confirms they are leaving the network
  //
  //
  // Packet Structure = B        BBBBBBBBBBBBBBBBBBB
  //                    Header   Message
  //
  //////////////////////////////////////////////////////////////////////////
  else if(header == networkBrokenConfirm)
  {
    return true;
  }
  //some unkown packet
  else
    return false;
}
//------------------------------------------------------------------------------------------------------------------


void tellChildrenImDead(void)
{
  byte message[5];
  message[0] = networkBroken;
  message[1] = nodeID[0];
  message[2] = nodeID[1];
  message[3] = nodeID[2];
  message[4] = nodeID[3];
  for(int i =0; i < numChildren; i++)
      writeOut(child_write[i], message, true);
  
  //delay for few minutes to allow network to cool down.
  delay(60000 + random(60000));
  inNetwork = false;
}

void checkNetworkState(void)
{
  //printf("Millis %lu\n\r",millis());
  //printf("networkStateTime %lu  -- Limit:%lu\n\r", networkStateTime, networkCheckTime*1);
  //printf("childCheckTime: %lu  -- Limit:%lu\n\r", checkTime, networkCheckTime*1);  
  //Check status of parent
  
  if(networkStateTime > networkCheckTime)
  {
    printf("DEAD - TIMEOUT\n\r");
    inNetwork = false;
    networkStateTime = 0;
    //Open reading pipe for network
    radio.openReadingPipe(0,newChan);
  }
  
  //Check status of children
  if(checkTime > networkCheckTime)
  {
    printf("Checking Children\n\r");
    checkTime = 0;
    for(int i = 0; i < sizeof(child_write)/sizeof(uint64_t); i++)
    {
       if(child_write[i] != 0 && child_status[i] == false)
       {
         numChildren -= 1;
         child_write[i] = 0;
         printf("Freeing Children\n\r");
       }
       else
       {
         child_status[i] = false; 
       }
   }
}
}

//Write a message to an address
boolean writeOut(uint64_t address, byte* message, boolean check)
{
      printf("Send Header = %u\n\r", message[0]);
      boolean done = true;
      
      radio.openWritingPipe(address);
      radio.stopListening();
      radio.write(message,32);
      radio.startListening();
      if(check)
      {
        int trys = 0;
        done = false;
        while(!done && trys <= 10)
        {
          trys += 1;
          delay(1000);
          while ( radio.available() )
          {
            //Read the message and send it to the processor function
            byte message[32];
            radio.read( &message, 32 );
            if(processMessage(message,check))
              done = true;
            if(done)
              break;
          }
          if(!done)
          {
            printf("Retry\n\r");
            radio.stopListening();
            radio.write(message,32);
            radio.startListening();
          }
        }
        if(trys > 10)
        {
            printf("DEAD - TIMEOUT\n\r");
            inNetwork = false;
        }
     }
     return done;
}


void powerDownRadio(void)
{
  radio.stopListening();
  radio.powerDown();
}
void powerUpRadio(void)
{
    radio.startListening();
}



