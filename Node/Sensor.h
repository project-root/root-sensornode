#include <Arduino.h>
#include <math.h>
#include <DHT.h>
//Constants
#define thermistorPin 3
#define voltageFlipPin1 4
#define voltageFlipPin2 5

#define DHTPIN 7
#define HumidityPower 2
#define DHTTYPE DHT22

#define Led 8

#define moistureSensorPin 2
#define groundTempPin 5

#define flipTimer 1000
//Sensor Check = seconds / 8 -> so 2 minutes = 120 seconds = 15
int sensorCheckTime = 112;
unsigned long sensorTime = 0;

//Variables
//unsigned long sensortime = 0;
boolean sendReading = false;


//Functions
void setupsensors(void);
void sendReadings(void);
void getReadings(int* airTemp, int* groundTemp, int* Humidity, int* Moisture);
int ReadMoistureSensor(int Pin);
int readVcc(void);
void setSensorPolarity(boolean flip);
double ReadThermistor(int pin);
