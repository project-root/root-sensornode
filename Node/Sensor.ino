void getReadings(byte* buffer)
{
   //////////////////////////////////////////////
   //
   // Packet Structure = B       BBBB    BBBB      BB BB BB BB BB BB BB BB BB BB          FF
   //                    Header  NodeID  ChildID   Payload
   //                                                    Air ~ Ground ~ Moist 1-10 ~ Reserved   End Byte
   //
   //////////////////////////////////////////////
    
    //header info
    buffer[0] = masterInfo;
    buffer[1] = nodeID[0];
    buffer[2] = nodeID[1];
    buffer[3] = nodeID[2];
    buffer[4] = nodeID[3];
    
    //Start the payload
    buffer[5] = nodeID[0];
    buffer[6] = nodeID[1];
    buffer[7] = nodeID[2];
    buffer[8] = nodeID[3];
    
    int airTemp;
    int groundTemp;
    int Humidity;
    int Moisture;
    int Voltage;
    
    getReadings(&airTemp, &groundTemp, &Humidity, &Moisture, &Voltage);
    
    Serial.println(airTemp); 
    Serial.println(groundTemp); 
    Serial.println(Humidity); 
    Serial.println(Moisture); 
    Serial.println(Voltage); 
     
    //Air Temp ex - 24
    buffer[9] = airTemp & 255;
    
    //Ground Temp ex - 65
    buffer[10] = groundTemp & 255;
    
    //Humidity ex - 42
    buffer[11] = Humidity & 255; 

    //Voltage ex - 1024
    buffer[12] = Voltage & 255;
    
    //Moisture ex - 1024
    buffer[13] = Moisture & 255;
    
    //Close Buffer with largest possible byte
    buffer[14] = 255;
}

void getReadings(int* airTemp, int* groundTemp, int* Humidity, int* Moisture, int* Voltage)
{
  digitalWrite(HumidityPower, HIGH);
  *Moisture = ReadMoistureSensor(moistureSensorPin);
  *Humidity = int(dht.readHumidity());
  *airTemp = int(dht.readTemperature(true));
  *groundTemp = int(ReadThermistor(groundTempPin));
  *Voltage = readVcc();
  digitalWrite(HumidityPower, LOW);
}

int readVcc() 
{
  int result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1184665L / result; // Back-calculate AVcc in mV
  return result/100;
}

int ReadMoistureSensor(int Pin)
{
  setSensorPolarity(true);
  delay(flipTimer);
  int val1 = analogRead(Pin);
  
  // invert the reading
  setSensorPolarity(false);
  delay(flipTimer);  
  int val2 = 1023 - analogRead(Pin);
  
  //sacle to 0-100
  long out = (((val1 + val2) / 2)*100)/1024;

  //Reset Pins
  digitalWrite(voltageFlipPin1, LOW);
  digitalWrite(voltageFlipPin2, LOW);
  if(out > -1)
  {
    return (int)out;
  }
  else
  {
    return ReadMoistureSensor(Pin);
  }
}

void setSensorPolarity(boolean flip)
{
  if(flip)
  {
    digitalWrite(voltageFlipPin1, HIGH);
    digitalWrite(voltageFlipPin2, LOW);
  }
  else
  {
    digitalWrite(voltageFlipPin1, LOW);
    digitalWrite(voltageFlipPin2, HIGH);
  }
}

double ReadThermistor(int pin) {
     digitalWrite(thermistorPin, HIGH);
     double Temp;
     Temp = log(10000.0*((1024.0/analogRead(pin)-1))); 
     Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
     Temp = Temp - 273.15;            // Convert Kelvin to Celcius
     Temp = (Temp * 9.0)/ 5.0 + 32.0; // Convert Celcius to Fahrenheit
     digitalWrite(thermistorPin, LOW);
     return Temp;
}
