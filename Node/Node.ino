#include "Node.h"
#include <DHT.h>


DHT dht(DHTPIN, DHTTYPE);

//#############################
//### SETUP ##################
//############################
void setup(void)
{
  Serial.begin(9600);
  radioSetup();
  //SENSOR
  dht.begin();
  pinMode(HumidityPower, OUTPUT);
  pinMode(voltageFlipPin1, OUTPUT);
  pinMode(voltageFlipPin2, OUTPUT);
  pinMode(thermistorPin, OUTPUT);
  pinMode(Led, OUTPUT);

  //force a reading to be sent right after we join the network
  //sendReading = true;
  //sensorTime = sensorCheckTime;
}

//#############################
//### MAIN LOGIC LOOP ########
//############################
void loop(void)
{
  //Mesh Layer Loop
  radioListner();
}
