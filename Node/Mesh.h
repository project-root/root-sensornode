//#############################
//### Constants ##############
//############################

//Different Message Headers
#define childDepthInfo 1
#define depthInfo 2
#define masterInfo 3
#define commandInfo 4
#define checkChildPathInfo 5
#define checkPathInfo 6
#define networkBroken 7
#define networkBrokenConfirm 8
#define imOn 9
#define maxChildren 4

//Sensor Check = seconds / 8 -> so 2 minutes = 120 seconds = 15
unsigned int networkCheckTime = 224;
unsigned long networkStateTime = 0;
unsigned long checkTime = 0;
byte networkStateFromParent;

//Different pipes
const uint64_t openChan = 0xF0F0F0F0A0LL;
const uint64_t newChan = 0xF0F0F0F0A1LL;

//#############################
//### Variables ##############
//############################

uint64_t addr;
uint64_t parent;
boolean child_status[4] = { false};
uint64_t child_read[4];
uint64_t child_write[4] = { 0 };
uint64_t prevtime = 0;

unsigned int randomNumber;

//Network state info
boolean inNetwork = false;
float wdtCalibration = 0.9;

//Node Info
byte nodeID[4];

byte numChildren = 0;
byte depth = -1; //this should overflow to the largest value because it is unsigned

//############################
//### Function prototypes ####
//############################

void tellChildrenImDead(void);
void radioSetup(void);
void radioListner(void);
boolean processMessage(byte* message,boolean check);
boolean writeOut(uint64_t address, byte* message,boolean check);
void checkNetworkState(void);
void powerDownRadio(void);
void powerUpRadio(void);

//############################
//### Helper Functions    ####
//############################

//ID Generator - needs input of byte* with 4 spaces
void generateID(byte* id)
{
  //shouldn't = 0x00         0xAA            0x55         0xFF
  id[0] = random(255);
  while(id[0] == 0 || id[0] == 170 || id[0] == 85 || id[0] == 255)
    id[0] = random(255);
  
  //Make sure we don't repeat bytes
  id[1] = random(255);
  while(id[1] == id[0])
    id[1] = random(255);
    
  id[2] = random(255);
  while(id[2] == id[1])
    id[2] = random(255);
    
  id[3] = random(255);
  while(id[3] == id[2])
    id[3] = random(255);
  
  //hardcode ID
  id[0] = 111; 
  id[1] = 113;    
  id[2] = 111;
  id[3] = 111;
  
}

//Write to Parent
boolean writeToParent(byte* message)
{
    return writeOut(parent,message,true);
}

//Write to all childs
void writeToChildren(byte* message)
{
    for(int i =0; i < numChildren; i++)
      writeOut(child_write[i], message, false);
}
 
//Write to Parent and Child (will probably never use this)
void writeToAll(byte* message)
{
    writeToParent(message);
    writeToChildren(message);
}

//Write to the open channel on the mesh network (used for initial adding)
void writeToOpen(byte* message)
{
    writeOut(openChan,message,false);
}

//Write to the open channel on the mesh network (used for initial adding)
void writeToNew(byte* message)
{
    writeOut(newChan,message,false);
}

//Good for debugging
void print_ull(uint64_t ll)
{
    uint64_t xx = ll/1000000000ULL;
    if (xx >0) Serial.print((long)xx);
    Serial.print((long)(ll-xx*1000000000));
    printf("\n\r");
}

//Make sure the byte array is not more than 8 bytes, otherwise we will return 0!
uint64_t b_to_ull(byte* array, byte inputSize)
{
  uint64_t out = 0;
  if(inputSize>8)
    return 0;
  for(int i=0; i < inputSize; i++)
     out = (out << 8) + array[i];
  return out;
}

//Convert a string to a unsigned long long
uint64_t s_to_ull(char* str)
{
    uint64_t out = 0;
    uint64_t newNum = 0;
    for(int i=0; i<strlen(str); i++)
    {
        switch(str[i])
        {
            case '0':
            newNum = 0;
            break;
            case '1':
            newNum = 1;
            break;
            case '2':
            newNum = 2;
            break;
            case '3':
            newNum = 3;
            break;
            case '4':
            newNum = 4;
            break;
            case '5':
            newNum = 5;
            break;
            case '6':
            newNum = 6;
            break;
            case '7':
            newNum = 7;
            break;
            case '8':
            newNum = 8;
            break;
            case '9':
            newNum = 9;
            break;
            case 'A':
            newNum = 10;
            break;
            case 'B':
            newNum = 11;
            break;
            case 'C':
            newNum = 12;
            break;
            case 'D':
            newNum = 13;
            break;
            case 'E':
            newNum = 14;
            break;
            case 'F':
            newNum = 15;
            break;
        }
        out = out * 16 + newNum;
    }
    return out;
}

void solidLight(void)
{
  digitalWrite(Led, HIGH);
}

void flashLight(void)
{
  digitalWrite(Led, HIGH);
  delay(15);
  digitalWrite(Led, LOW);
}

void noLight(void)
{
  digitalWrite(Led, LOW);
}
